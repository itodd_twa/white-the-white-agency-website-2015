/* globals App, google */

App.map = (function($) {
    'use strict';

    function initMap() {
        var $map = $('[data-white-map]');
        var mapStyles = [
            {
                'featureType': 'transit.line',
                'elementType': 'labels',
                'stylers': [
                    { 'visibility': 'off' }
                ]
            }
        ];
        var map;
        var marker;

        if ($map.length) {
            map = new google.maps.Map($('[data-white-map]')[0],
                {
                    center: { lat: -33.857929, lng: 151.203270 },
                    draggable: !App.utils.isMobile(),
                    scrollwheel: false,
                    zoom: 16,
                    mapTypeControl: false,
                    styles: mapStyles
                });

            marker = new google.maps.Marker({
                position: { lat: -33.857929, lng: 151.203270 },
                title: 'The White Agency'
            });

            marker.setMap(map);
        } else {
            return false;
        }
    }

    return {
        init: initMap
    };
})(jQuery);