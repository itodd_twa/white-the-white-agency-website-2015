/* eslint no-unused-vars: 0 */

var App = (function() {
    'use strict';

    var utils = {

        /**
         * Not good to user agent sniffing, but unable to do feature test for youtube autoplay on
         * ios devices OR for the iOS transform-origin bug.
         */
        isIos: function() {
            return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        },

        isMobile: function() {
            return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
        }
    };

    return {
        utils: utils
    };
}());


$(function() {
    'use strict';

    // Initialise load more
    $('.js-white-load-more').whiteLoadMore({
        amount: 2,
        data: 'load-more.json',
        fadeIn: 500
    });
});
(function($) {
    'use strict';

    // Add/Remove header background
    var userScrolled = false;

    $(window).scroll(function() {
        userScrolled = true;
    });

    // Use setIntervbal to improve performace when user scrolls
    setInterval(function() {
        var offset = $('#content').offset().top - 62;
        var scroll = window.pageYOffset || document.documentElement.scrollTop;
        var $header = $('header');

        if (userScrolled) {
            if (scroll > offset) {
                $header.addClass('header--bg');
            } else {
                $header.removeClass('header--bg');
            }
            userScrolled = false;
        }
    }, 50);

})(jQuery);
/* globals App */

(function($) {
    'use strict';

    var $hero = $('.js-banner--hero');
    var percent = $hero.attr('data-hero-height');
    var height = percent ? (($(window).height() / 100) * parseInt(percent)) : $(window).height();

    if (App.utils.isMobile()) {
        $hero.height(height);
    }
})(jQuery);
(function($) {
    'use strict';

    $('.js-jump').on('click', function(e) {
        var $this = $(this);
        var $target = $this.is('a[href*="#"]') ? $($this.attr('href')) : [];
        var speed = $this.attr('data-jump-speed');
        var animate = speed === undefined ? 700 : parseInt(speed);
        var offset = $target.offset().top - $('.header').outerHeight();

        if ($target.length === 1 && animate) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: offset
            }, animate, function() {

                // TODO [IAIN TODD]: Find solution for IE to stop flicker
                // $target.focus();
                // window.scroll(0, offset);
            });
        }
    });

})(jQuery);
/* globals Handlebars */

(function($) {
    'use strict';

    $.fn.whiteLoadMore = function(o) {

        var defaults = {
            amount: 'all',
            callback: false,
            contentSelector: '.js-white-load-more-content',
            data: false,
            dataJSONProperty: 'content',
            event: 'click',
            fadeIn: false, // Avoid using fadeIn when loading many DOM elements.
            hideTriggerAtEnd: true,
            loadingClass: 'white-load-more--loading',
            startAt: 0,
            tmplSelector: '#js-white-load-more-tmpl'
        };

        // Exit if Handlebars doesn't exist
        if (typeof Handlebars === 'undefined') {
            throw new Error('This plugin requires Handlebars.');
        }

        return this.each(function() {
            var _this = this;
            var $this = $(_this);

            // Allow options to be set via data-load-more-options attribute.
            // Example: data-load-more-options='{"data": "more.json"}'
            var dataOptions = typeof $this.data().loadMoreOptions === 'object' ? $this.data().loadMoreOptions : {};

            // Extend default options with those when initialised and from data attribute
            var options = $.extend({}, defaults, o, dataOptions);

            var cachedData;
            var dataSrc = options.data;
            var dataType = typeof dataSrc;
            var startAt = options.startAt;

            // Stateful object
            $this.data('loadMore', {
                reachedEnd: false
            });

            function loadItems(_data) {
                var elements = '';
                var tmplSource = $(options.tmplSelector).html();
                var tmpl = Handlebars.compile(tmplSource);
                var totalItems = _data.content.length;
                var loadTo = options.amount % 1 === 0 &&
                             options.amount < totalItems ? options.amount : totalItems;

                // Cap loadTo at total amount of items
                loadTo = Math.min(loadTo += startAt, totalItems);

                for (; startAt < loadTo; startAt++) {

                    // If fadeIn then append elements as jquery objects, otherwise add the string
                    // and append string after the loop
                    if (options.fadeIn) {
                        $(tmpl(_data[options.dataJSONProperty][startAt]))
                            .hide()
                            .appendTo(options.contentSelector)
                            .fadeIn(options.fadeIn);
                    } else {
                        elements += tmpl(_data[options.dataJSONProperty][startAt]);
                    }

                    // If this is the last item in the data
                    if (startAt === totalItems - 1) {
                        $this.data('loadMore').reachedEnd = true;

                        if (options.hideTriggerAtEnd) {
                            $this.hide();
                        }
                    }
                }

                if (!options.fadeIn) {
                    $(options.contentSelector).append(elements);
                }

                // Callback
                if (typeof options.callback === 'function') options.callback.call(_this);

            }

            function getData() {
                if (cachedData) {
                    loadItems(cachedData);
                } else if (dataType === 'object') {
                    cachedData = dataSrc;
                    loadItems(cachedData);
                } else if (dataType === 'string' && dataSrc.split('.').pop() === 'json') {

                    // Add loading class
                    $this.addClass(options.loadingClass);

                    $.ajax({
                        url: dataSrc,
                        dataType: 'json'
                    })
                    .done(function(result) {
                        cachedData = result;
                        loadItems(cachedData);
                    })
                    .fail(function() {

                        // TODO [IAIN TODD]: Handle failed request?
                    })
                    .always(function() {

                        // Remove loading class
                        $this.removeClass(options.loadingClass);
                    });
                }
            }

            $this.on(options.event + ' whiteLoadMore', function() {
                getData();
            });
        });
    };
})(jQuery);
/* globals App, google */

App.map = (function($) {
    'use strict';

    function initMap() {
        var $map = $('[data-white-map]');
        var mapStyles = [
            {
                'featureType': 'transit.line',
                'elementType': 'labels',
                'stylers': [
                    { 'visibility': 'off' }
                ]
            }
        ];
        var map;
        var marker;

        if ($map.length) {
            map = new google.maps.Map($('[data-white-map]')[0],
                {
                    center: { lat: -33.857929, lng: 151.203270 },
                    draggable: !App.utils.isMobile(),
                    scrollwheel: false,
                    zoom: 16,
                    mapTypeControl: false,
                    styles: mapStyles
                });

            marker = new google.maps.Marker({
                position: { lat: -33.857929, lng: 151.203270 },
                title: 'The White Agency'
            });

            marker.setMap(map);
        } else {
            return false;
        }
    }

    return {
        init: initMap
    };
})(jQuery);
(function($) {
    'use strict';

    // Cache Menu toggle elements
    var $body = $('body');
    var $menu = $('[data-menu]');
    var $toggleBtn = $('[data-menu-toggle]');
    var $menuList = $('.js-menu__list');
    var $menuItemsWithHash = $menuList.find('a[href*=#]');

    // Menu toggle functionality
    $toggleBtn.on('click clickMenuToggle', function() {
        var scroll = $body.css('top') !== 'auto' ? $body.css('top') : (window.pageYOffset || document.documentElement.scrollTop);

        if ($body.attr('data-menu-visible') === 'true') {
            $body.css('top', '');

            $(document).off('keyup.escapeClose');

            $toggleBtn
                .attr('aria-expanded', false)
                .focus();

            $menu
                .stop()
                .fadeOut();
            $body.removeAttr('data-menu-visible');
            window.scroll(0, Math.abs(parseInt(scroll)));

        } else {
            $body.css('top', '-' + scroll + 'px');

            // Close menu when escape key is pressed
            $(document).on('keyup.escapeClose', function(e) {
                if (e.keyCode === 27 && $body.is('[data-menu-visible]')) {
                    $menu.stop();
                    $toggleBtn.trigger('clickMenuToggle');
                }
            });

            $toggleBtn.attr('aria-expanded', true);

            $menu
                .stop()
                .fadeIn()
                .css('display', 'table');
            $body.attr('data-menu-visible', 'true');
        }
    })
    .attr('aria-expanded', false);

    // When clicking on menu item with a same page hash
    $menuItemsWithHash.on('click', function(e) {
        var $this = $(this);
        var linkHref = $this.attr('href');
        var hash = linkHref.split('#')[1];
        var currentPage = location.pathname.split('/').slice(-1)[0];
        var $target = $('#' + hash).length ? $('#' + hash) : $('[name="' + hash + '"]');

        // If clicked link is the current page
        if (linkHref.indexOf(currentPage) > -1) {
            e.preventDefault();

            // Close menu
            $toggleBtn.trigger('clickMenuToggle');

            // Scroll page to target
            $('html, body').scrollTop($target.offset().top);

            // Set URL hash
            window.location.hash = hash;
        }
    });

})(jQuery);
/* globals $, App, YT*/

App.videoManager = (function() {
    'use strict';

    var players = [];

    function onPlayerReady(event) {
        event.target.playerIsReady = true;
    }

    function createYouTubePlayer($container) {
        var player = new YT.Player($container.find('.video-player-container').attr('id'), {
            videoId: $container.attr('data-you-tube-id'),
            playerVars: {
                showinfo: 0,
                modestbranding: 1,
                rel: 0
            },
            events: {
                'onReady': onPlayerReady
            }
        });

        $container.attr('data-allow-cube', !App.utils.isIos());
        player.$container = $container;

        return player;
    }

    function updateTransformOrigins() {
        players.forEach(function(player) {
            var zValue;

            if (!App.utils.isIos()) {
                zValue = -player.$container.width() / 2;
                player.$container.find('.video-cube-face')
                    .css('-webkit-transform-origin', 'center center ' + zValue + 'px')
                    .css('transform-origin', 'center center ' + zValue + 'px');
            }
        });
    }

    function createYouTubePlayers() {

        // Effort to avoid some devices randomly requiring a double tap.
        setTimeout(function() {
            $('[data-video-component]').each(function() {
                var player = createYouTubePlayer($(this));
                players.push(player);

                $(this).on('click', function() {
                    $(this).attr('data-video-visible', true);
                    player.playVideo();
                });
            });

            updateTransformOrigins();
        }, 100);
    }

    function appendYouTubeApi() {
        var tag;
        var firstScriptTag;

        window.onYouTubeIframeAPIReady = createYouTubePlayers;

        tag = document.createElement('script');
        tag.src = 'https://www.youtube.com/iframe_api';
        firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    function initialize() {
        appendYouTubeApi();
        $(window).on('resize', updateTransformOrigins);
    }

    initialize();

    return {
        createYouTubePlayers: createYouTubePlayers
    };
}());