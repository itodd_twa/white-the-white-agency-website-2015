/* eslint no-unused-vars: 0 */

var App = (function() {
    'use strict';

    var utils = {

        /**
         * Not good to user agent sniffing, but unable to do feature test for youtube autoplay on
         * ios devices OR for the iOS transform-origin bug.
         */
        isIos: function() {
            return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        },

        isMobile: function() {
            return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
        }
    };

    return {
        utils: utils
    };
}());


$(function() {
    'use strict';

    // Initialise load more
    $('.js-white-load-more').whiteLoadMore({
        amount: 2,
        data: 'load-more.json',
        fadeIn: 500
    });
});