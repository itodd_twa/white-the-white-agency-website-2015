/* globals App */

(function($) {
    'use strict';

    var $hero = $('.js-banner--hero');
    var percent = $hero.attr('data-hero-height');
    var height = percent ? (($(window).height() / 100) * parseInt(percent)) : $(window).height();

    if (App.utils.isMobile()) {
        $hero.height(height);
    }
})(jQuery);