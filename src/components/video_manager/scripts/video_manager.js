/* globals $, App, YT*/

App.videoManager = (function() {
    'use strict';

    var players = [];

    function onPlayerReady(event) {
        event.target.playerIsReady = true;
    }

    function createYouTubePlayer($container) {
        var player = new YT.Player($container.find('.video-player-container').attr('id'), {
            videoId: $container.attr('data-you-tube-id'),
            playerVars: {
                showinfo: 0,
                modestbranding: 1,
                rel: 0
            },
            events: {
                'onReady': onPlayerReady
            }
        });

        $container.attr('data-allow-cube', !App.utils.isIos());
        player.$container = $container;

        return player;
    }

    function updateTransformOrigins() {
        players.forEach(function(player) {
            var zValue;

            if (!App.utils.isIos()) {
                zValue = -player.$container.width() / 2;
                player.$container.find('.video-cube-face')
                    .css('-webkit-transform-origin', 'center center ' + zValue + 'px')
                    .css('transform-origin', 'center center ' + zValue + 'px');
            }
        });
    }

    function createYouTubePlayers() {

        // Effort to avoid some devices randomly requiring a double tap.
        setTimeout(function() {
            $('[data-video-component]').each(function() {
                var player = createYouTubePlayer($(this));
                players.push(player);

                $(this).on('click', function() {
                    $(this).attr('data-video-visible', true);
                    player.playVideo();
                });
            });

            updateTransformOrigins();
        }, 100);
    }

    function appendYouTubeApi() {
        var tag;
        var firstScriptTag;

        window.onYouTubeIframeAPIReady = createYouTubePlayers;

        tag = document.createElement('script');
        tag.src = 'https://www.youtube.com/iframe_api';
        firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    function initialize() {
        appendYouTubeApi();
        $(window).on('resize', updateTransformOrigins);
    }

    initialize();

    return {
        createYouTubePlayers: createYouTubePlayers
    };
}());