(function($) {
    'use strict';

    // Cache Menu toggle elements
    var $body = $('body');
    var $menu = $('[data-menu]');
    var $toggleBtn = $('[data-menu-toggle]');
    var $menuList = $('.js-menu__list');
    var $menuItemsWithHash = $menuList.find('a[href*=#]');

    // Menu toggle functionality
    $toggleBtn.on('click clickMenuToggle', function() {
        var scroll = $body.css('top') !== 'auto' ? $body.css('top') : (window.pageYOffset || document.documentElement.scrollTop);

        if ($body.attr('data-menu-visible') === 'true') {
            $body.css('top', '');

            $(document).off('keyup.escapeClose');

            $toggleBtn
                .attr('aria-expanded', false)
                .focus();

            $menu
                .stop()
                .fadeOut();
            $body.removeAttr('data-menu-visible');
            window.scroll(0, Math.abs(parseInt(scroll)));

        } else {
            $body.css('top', '-' + scroll + 'px');

            // Close menu when escape key is pressed
            $(document).on('keyup.escapeClose', function(e) {
                if (e.keyCode === 27 && $body.is('[data-menu-visible]')) {
                    $menu.stop();
                    $toggleBtn.trigger('clickMenuToggle');
                }
            });

            $toggleBtn.attr('aria-expanded', true);

            $menu
                .stop()
                .fadeIn()
                .css('display', 'table');
            $body.attr('data-menu-visible', 'true');
        }
    })
    .attr('aria-expanded', false);

    // When clicking on menu item with a same page hash
    $menuItemsWithHash.on('click', function(e) {
        var $this = $(this);
        var linkHref = $this.attr('href');
        var hash = linkHref.split('#')[1];
        var currentPage = location.pathname.split('/').slice(-1)[0];
        var $target = $('#' + hash).length ? $('#' + hash) : $('[name="' + hash + '"]');

        // If clicked link is the current page
        if (linkHref.indexOf(currentPage) > -1) {
            e.preventDefault();

            // Close menu
            $toggleBtn.trigger('clickMenuToggle');

            // Scroll page to target
            $('html, body').scrollTop($target.offset().top);

            // Set URL hash
            window.location.hash = hash;
        }
    });

})(jQuery);