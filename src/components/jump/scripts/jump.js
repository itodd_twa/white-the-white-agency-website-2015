(function($) {
    'use strict';

    $('.js-jump').on('click', function(e) {
        var $this = $(this);
        var $target = $this.is('a[href*="#"]') ? $($this.attr('href')) : [];
        var speed = $this.attr('data-jump-speed');
        var animate = speed === undefined ? 700 : parseInt(speed);
        var offset = $target.offset().top - $('.header').outerHeight();

        if ($target.length === 1 && animate) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: offset
            }, animate, function() {

                // TODO [IAIN TODD]: Find solution for IE to stop flicker
                // $target.focus();
                // window.scroll(0, offset);
            });
        }
    });

})(jQuery);