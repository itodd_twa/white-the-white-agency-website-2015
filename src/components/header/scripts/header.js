(function($) {
    'use strict';

    // Add/Remove header background
    var userScrolled = false;

    $(window).scroll(function() {
        userScrolled = true;
    });

    // Use setIntervbal to improve performace when user scrolls
    setInterval(function() {
        var offset = $('#content').offset().top - 62;
        var scroll = window.pageYOffset || document.documentElement.scrollTop;
        var $header = $('header');

        if (userScrolled) {
            if (scroll > offset) {
                $header.addClass('header--bg');
            } else {
                $header.removeClass('header--bg');
            }
            userScrolled = false;
        }
    }, 50);

})(jQuery);