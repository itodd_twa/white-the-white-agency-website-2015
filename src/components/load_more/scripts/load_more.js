/* globals Handlebars */

(function($) {
    'use strict';

    $.fn.whiteLoadMore = function(o) {

        var defaults = {
            amount: 'all',
            callback: false,
            contentSelector: '.js-white-load-more-content',
            data: false,
            dataJSONProperty: 'content',
            event: 'click',
            fadeIn: false, // Avoid using fadeIn when loading many DOM elements.
            hideTriggerAtEnd: true,
            loadingClass: 'white-load-more--loading',
            startAt: 0,
            tmplSelector: '#js-white-load-more-tmpl'
        };

        // Exit if Handlebars doesn't exist
        if (typeof Handlebars === 'undefined') {
            throw new Error('This plugin requires Handlebars.');
        }

        return this.each(function() {
            var _this = this;
            var $this = $(_this);

            // Allow options to be set via data-load-more-options attribute.
            // Example: data-load-more-options='{"data": "more.json"}'
            var dataOptions = typeof $this.data().loadMoreOptions === 'object' ? $this.data().loadMoreOptions : {};

            // Extend default options with those when initialised and from data attribute
            var options = $.extend({}, defaults, o, dataOptions);

            var cachedData;
            var dataSrc = options.data;
            var dataType = typeof dataSrc;
            var startAt = options.startAt;

            // Stateful object
            $this.data('loadMore', {
                reachedEnd: false
            });

            function loadItems(_data) {
                var elements = '';
                var tmplSource = $(options.tmplSelector).html();
                var tmpl = Handlebars.compile(tmplSource);
                var totalItems = _data.content.length;
                var loadTo = options.amount % 1 === 0 &&
                             options.amount < totalItems ? options.amount : totalItems;

                // Cap loadTo at total amount of items
                loadTo = Math.min(loadTo += startAt, totalItems);

                for (; startAt < loadTo; startAt++) {

                    // If fadeIn then append elements as jquery objects, otherwise add the string
                    // and append string after the loop
                    if (options.fadeIn) {
                        $(tmpl(_data[options.dataJSONProperty][startAt]))
                            .hide()
                            .appendTo(options.contentSelector)
                            .fadeIn(options.fadeIn);
                    } else {
                        elements += tmpl(_data[options.dataJSONProperty][startAt]);
                    }

                    // If this is the last item in the data
                    if (startAt === totalItems - 1) {
                        $this.data('loadMore').reachedEnd = true;

                        if (options.hideTriggerAtEnd) {
                            $this.hide();
                        }
                    }
                }

                if (!options.fadeIn) {
                    $(options.contentSelector).append(elements);
                }

                // Callback
                if (typeof options.callback === 'function') options.callback.call(_this);

            }

            function getData() {
                if (cachedData) {
                    loadItems(cachedData);
                } else if (dataType === 'object') {
                    cachedData = dataSrc;
                    loadItems(cachedData);
                } else if (dataType === 'string' && dataSrc.split('.').pop() === 'json') {

                    // Add loading class
                    $this.addClass(options.loadingClass);

                    $.ajax({
                        url: dataSrc,
                        dataType: 'json'
                    })
                    .done(function(result) {
                        cachedData = result;
                        loadItems(cachedData);
                    })
                    .fail(function() {

                        // TODO [IAIN TODD]: Handle failed request?
                    })
                    .always(function() {

                        // Remove loading class
                        $this.removeClass(options.loadingClass);
                    });
                }
            }

            $this.on(options.event + ' whiteLoadMore', function() {
                getData();
            });
        });
    };
})(jQuery);